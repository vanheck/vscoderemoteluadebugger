import * as vscode from 'vscode';
import { WorkspaceFolder, DebugConfiguration, ProviderResult, CancellationToken } from 'vscode';
import { LuaDebugSession } from './luaDebugSession';
import * as Net from 'net';

const debuggerType = "lua-remote";
const EMBED_DEBUG_ADAPTER = true;
//const EMBED_DEBUG_ADAPTER = true; // Extension development and debugging

class LuaRemoteConfigurationProvider implements vscode.DebugConfigurationProvider {
	
	/**
	 * Massage a debug configuration just before a debug session is being launched,
	 * e.g. add all missing attributes to the debug configuration.
	 */
	resolveDebugConfiguration?(folder: WorkspaceFolder | undefined, config: DebugConfiguration, token?: CancellationToken): ProviderResult<DebugConfiguration>{
		// if launch.json is missing or empty
		if (!config.type && !config.request && !config.name) {
			const editor = vscode.window.activeTextEditor;
			if (editor && editor.document.languageId === 'lua') {
				config.type = "lua-remote";
				config.name = 'Start-Lua-Debug-Server';
				config.request = 'launch';
				config.rootFolder = '${workspaceFolder}';
				config.debugServerIp = '0.0.0.0';
				config.port = 8172;
				config.stopOnEntry = true;
			}
		}

		return config;
	}
}

class LuaDebugAdapterDescriptorFactory implements vscode.DebugAdapterDescriptorFactory {

	private server?: Net.Server;

	createDebugAdapterDescriptor(session: vscode.DebugSession, executable: vscode.DebugAdapterExecutable | undefined): vscode.ProviderResult<vscode.DebugAdapterDescriptor> {

		if (!this.server) {
			// start listening on a random port
			this.server = Net.createServer(socket => {
				const session = new LuaDebugSession();
				session.setRunAsServer(true);
				session.start(<NodeJS.ReadableStream>socket, socket);
			}).listen(0);
		}

		// make VS Code connect to debug server
		return new vscode.DebugAdapterServer((<Net.AddressInfo>this.server.address()).port);
	}

	dispose() {
		if (this.server) {
			this.server.close();
		}
	}
}

// activating extension ("main" function)
export function activate(context: vscode.ExtensionContext) {

	// DEBUG - pak smazat
	console.log('Lua remote debuger extension is now available!');

	const provider = new LuaRemoteConfigurationProvider();
	context.subscriptions.push(vscode.debug.registerDebugConfigurationProvider(debuggerType, provider));
	
	if (EMBED_DEBUG_ADAPTER) {
		const factory = new LuaDebugAdapterDescriptorFactory();
		context.subscriptions.push(vscode.debug.registerDebugAdapterDescriptorFactory(debuggerType, factory));
		context.subscriptions.push(factory);
	}
}

// deactivating extension
export function deactivate() {}