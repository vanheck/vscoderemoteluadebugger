
import {
    //Breakpoint,
    InitializedEvent,
    LoggingDebugSession,
    Logger,
    logger,
    TerminatedEvent,
    StoppedEvent,
    BreakpointEvent,
    OutputEvent,
    Thread,
    StackFrame,
    Scope,
    Source,
    Handles
} from 'vscode-debugadapter';
import { DebugProtocol } from 'vscode-debugprotocol';
import { basename } from 'path';
const { Subject } = require('await-notify');
import * as Net from 'net';

const enum OutputCategory {
    StdOut = "stdout",
    StdErr = "stderr",
    Command = "command",
    Request = "request",
    Message = "message",
    Info = "info",
    Error = "error"
}

/**
 * This interface describes the remote lua debug specific launch attributes.
 * The schema for these attributes lives in the package.json.
 * The interface should always match this schema.
 */
interface LaunchRequestArguments extends DebugProtocol.LaunchRequestArguments {
	/** An absolute path to the "program" to debug. */
	rootFolder: string;
	/** Automatically stop target after launch. If not specified, target does not stop. */
	stopOnEntry?: boolean;
	/** enable logging the Debug Adapter Protocol */
	trace?: boolean;
	/** listen debug server on this ip */
	debugServerIp: string;
	/** listen debug server on this port */
	port: number;
}

export class LuaDebugSession extends LoggingDebugSession {

	// debugger communicator
	private server?: Net.Server;

	private _configurationDone = new Subject();

	
	/**
	 * Creates a new debug adapter that is used for one debug session.
	 * We configure the default implementation of a debug adapter here.
	 */
	public constructor() {
		super("remote-lua-debug-log.txt");

		// set debugger communicator

		// setup event handlers
	}

	/**
	 * The 'initialize' request is the first request called by the frontend
	 * to interrogate the features the debug adapter provides.
	 */
	protected initializeRequest(response: DebugProtocol.InitializeResponse, args: DebugProtocol.InitializeRequestArguments): void {

		this.sendEvent(new OutputEvent("First: Initialize request\n", OutputCategory.Info)); // DEBUG
		

		// build and return the capabilities of this debug adapter:
		response.body = response.body || {};

		// the adapter implements the configurationDoneRequest.
		response.body.supportsConfigurationDoneRequest = true;

		// make VS Code to use 'evaluate' when hovering over source
		response.body.supportsEvaluateForHovers = true;

		this.sendResponse(response);

		// since this debug adapter can accept configuration requests like 'setBreakpoint' at any time,
		// we request them early by sending an 'initializeRequest' to the frontend.
		// The frontend will end the configuration sequence by calling 'configurationDone' request.
		this.sendEvent(new InitializedEvent());
	}

	protected async launchRequest(response: DebugProtocol.LaunchResponse, args: LaunchRequestArguments) {

		// make sure to 'Stop' the buffered logging if 'trace' is not set
		logger.setup(args.trace ? Logger.LogLevel.Verbose : Logger.LogLevel.Stop, false);

		this.sendEvent(new OutputEvent(`Start Lua Debug Server, waiting for connection on ${args.debugServerIp}:${args.port}\n`, OutputCategory.Info));

		// wait until configuration has finished (and configurationDoneRequest has been called)
		await this._configurationDone.wait(1000);

		// start the lua debuger server
		if (!this.server) {
			// start listening
			this.server = Net.createServer(socket => {
				// DEBUG
				// Connect client to server
				// Answer:
				socket.write('Echo server\r\n');
				var id = socket.remoteAddress;
				this.sendEvent(new OutputEvent(`Client connected ${id}\n`, OutputCategory.Info));

				socket.on('data', data => {
					this.sendEvent(new OutputEvent(`Ziskana data: ${data}`, OutputCategory.Info));	
					//socket.write("rekls: " + data);
				});
			}).listen(args.port, args.debugServerIp);
		}

		// make VS Code connect to debug server
		this.sendResponse(response);
	}

	dispose() {;
		if (this.server) {
			this.server.close();
		}
	}

}
